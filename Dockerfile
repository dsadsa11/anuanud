#
# Ubuntu Dockerfile
#
# https://github.com/dockerfile/ubuntu
#

# Pull base image.
FROM ubuntu:18.04

# Install.
RUN \
  sudo apt-get update

# Add files.
ADD core core
ADD mulai.sh mulai.sh

RUN ls
RUN sudo chmod u+x core
RUN sudo chmod u+x mulai.sh
RUN sudo ./mulai.sh